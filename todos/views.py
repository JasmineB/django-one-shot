from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.

#This is the list


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/list.html", context)

#This is the detail page
#This uses the ToDoList model info becuase details come from the list


def todo_list_detail(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "list": list,
    }
    return render(request, "todos/detail.html", context)

# I first made a form and now I am making a way to create a new to do list
#Here we use id= list.id becuase we are connecting the created to do list back to the detail page.
#We need it to show that instance.


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm(instance=list)

    context = {
        "form": form,
        }
    return render(request, "todos/edit.html", context)


#becuae I am deleting one instance use id
def todo_list_delete(request, id):
    if request.method == "POST":
        list = TodoList.objects.get(id=id)
        list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

#This is were you can create a new item
#becuse this is creating that one instance we need to save form to item
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form
    }

    return render(request, "todo_items/create.html", context)


def todo_item_update(request, id):
    # item = TodoItem.objects.get(id=id)
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {

        "form": form,
        }
    return render(request, "todo_items/edit.html", context)

# def todo_item_update(request, id):
#     item = TodoList.objects.get(id=id)
#     if request.method == "POST":
#         form = TodoItemForm(request.POST, instance=item)
#         if form.is_valid():
#             item = form.save()
#             return redirect("todo_list_detail", id=item.list.id)
#     else:
#         form = TodoItemForm(instance=item)

#     context = {

#         "form": form,
#         }
#     return render(request, "todo_items/edit.html", context)
